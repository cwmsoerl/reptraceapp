package Controllers;

import android.content.ContentValues;
import android.content.Context;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseData extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "reptrace.sqlite";
    public static final String VEHICLES_TABLE = "vehicles";
    public static final String ID = "id";
    public static final String BRAND = "brand";
    public static final String LICENSEPLATE = "licenseplate";
    public static final String LABEL = "label";
    public static final String YEAR = "year";

    private static final String CREATE_TABLE_VEHICLES = "CREATE TABLE "
                        + VEHICLES_TABLE + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + BRAND
                        + " TEXT," + LICENSEPLATE + " TEXT," + LABEL + " TEXT,"
                       + YEAR + " TEXT" + ")";

    private static final String LOG_TAG = DatabaseData.class.getSimpleName();


    public DatabaseData(Context context) {
        super(context, DATABASE_NAME , null, 1);
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d(LOG_TAG,"Created database");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table vehicles " +
                        "(id integer primary key autoincrement, brand text,licenseplate text,label text, year text)"
        );
        Log.d(LOG_TAG,"Created tables");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS vehicles");
        onCreate(db);
    }

    public boolean insertVehicle (String brand, String licenseplate, String label, String year) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(BRAND,brand);
        contentValues.put(LICENSEPLATE,licenseplate);
        contentValues.put(LABEL,label);
        contentValues.put(YEAR,year);
        long result = db.insert(VEHICLES_TABLE, null,contentValues);

        return result != -1;

    }

}
