package com.example.frank.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ToggleButton;

import com.example.frank.AndroidTests.R;

public class AppSettingsActivity extends AppCompatActivity {

    private static final String LOG_TAG = AppSettingsActivity.class.getSimpleName();

    private ConstraintLayout backgroundAppSettings;
    private ConstraintLayout backgroundMain;


    private ToggleButton toggleThemeButton;

     public void onCreate(final Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.app_settings);

         backgroundAppSettings = (ConstraintLayout) findViewById(R.id.appSettingsLayout);
         backgroundMain = (ConstraintLayout) findViewById(R.id.mainLayout);

         toggleThemeButton = (ToggleButton) findViewById(R.id.toggleThemeButton);
         toggleThemeButton.setOnClickListener(new View.OnClickListener() {

             @Override
             public void onClick(View arg0) {

                 if (toggleThemeButton.isChecked()) {
                     backgroundAppSettings.setBackgroundColor(Color.DKGRAY);
                     /* setTheme(R.style.SwitchBackground);
                     backgroundMain.setBackgroundColor(Color.DKGRAY); */
                 } else
                     backgroundAppSettings.setBackgroundColor(Color.WHITE);
                     /* setTheme(R.style.AppTheme);
                     backgroundMain.setBackgroundColor(Color.WHITE); */
             }
         });
     }

    public void MainActivity(View view) {
        Log.d(LOG_TAG, "Close Button App Setting clicked!");
        Intent mainActivity = new Intent(this, MainActivity.class);
        startActivity(mainActivity);
    }
}
