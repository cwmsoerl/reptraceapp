package com.example.frank.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.frank.AndroidTests.R;

public class EnvironmentalZonesActivity extends AppCompatActivity {

    private static final String LOG_TAG = EnvironmentalZonesActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.environment_zones);
    }

    public void MainActivity(View view) {
        Log.d(LOG_TAG, "Close Button Environment Zones clicked!");
        Intent mainActivity = new Intent(this, MainActivity.class);
        startActivity(mainActivity);
    }
}
