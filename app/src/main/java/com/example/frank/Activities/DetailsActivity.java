package com.example.frank.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.frank.AndroidTests.R;

public class DetailsActivity extends AppCompatActivity {

    private static final String LOG_TAG = VehiclesActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

    }

    public void VehicleActivity(View view) {
        Log.d(LOG_TAG, "Back Button Vehicle Details clicked!");
        Intent vehicleActivity = new Intent(this, VehiclesActivity.class);
        startActivity(vehicleActivity);
    }

}
