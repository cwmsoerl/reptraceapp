package com.example.frank.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.example.frank.AndroidTests.R;

import Controllers.DatabaseData;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    DatabaseData reptraceDb;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        reptraceDb = new DatabaseData(this);

    }

    public void EnvironmentZonesActivity(View view) {
        Log.d(LOG_TAG, "Button Environment Zones clicked!");
        Intent environmentZonesActivity = new Intent(this, EnvironmentalZonesActivity.class);
        startActivity(environmentZonesActivity);

    }

    public void VehicleActivity(View view) {
        Log.d(LOG_TAG, "Button Vehicle clicked!");
        Intent vehicleActivity = new Intent(this, VehiclesActivity.class);
        startActivity(vehicleActivity);

    }

    public void AppSettingsActivity(View view) {
        Log.d(LOG_TAG, "Button App Settings clicked!");
        Intent appSettingsActivity = new Intent(this, AppSettingsActivity.class);
        startActivity(appSettingsActivity);

    }

    public void RechargeStationsActivity(View view) {
        Log.d(LOG_TAG, "Button Recharge Stations clicked!");
        Intent rechargeStationsActivity = new Intent(this, RechargeStationsActivity.class);
        startActivity(rechargeStationsActivity);
    }
}
