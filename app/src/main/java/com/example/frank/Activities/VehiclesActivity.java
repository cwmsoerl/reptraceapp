package com.example.frank.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.example.frank.AndroidTests.R;
import Controllers.DatabaseData;


public class VehiclesActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final String LOG_TAG = VehiclesActivity.class.getSimpleName();
    private DatabaseData reptraceDb ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicle);

    }

    public void MainActivity(View view) {
        Log.d(LOG_TAG, "Close Button Vehicle clicked!");
        Intent mainActivity = new Intent(this, MainActivity.class);
        startActivity(mainActivity);
    }

    public void AddVehicleActivity(View view) {
        Log.d(LOG_TAG, "Add Vehicle Button clicked!");
        Intent addVehicleActivity = new Intent(this, AddVehicleActivity.class);
        startActivity(addVehicleActivity);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, DetailsActivity.class);
        startActivity(intent);
    }
}



