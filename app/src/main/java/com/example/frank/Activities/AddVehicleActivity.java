package com.example.frank.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import com.example.frank.AndroidTests.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import Controllers.DatabaseData;

public class AddVehicleActivity extends AppCompatActivity {

    private static final String LOG_TAG = AddVehicleActivity.class.getSimpleName();

    private Button addPictureButton;
    private Button saveVehicleButton;
    private ImageView imageViewPicture;

    DatabaseData reptraceDb;
    EditText editBrandText, editLicenseplateText, editLabelText, editYearText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicle);
        reptraceDb = new DatabaseData(this);

        addPictureButton = (Button) findViewById(R.id.cameraButton);
        imageViewPicture = (ImageView) findViewById(R.id.vehicleImageView);
        editBrandText = (EditText)findViewById(R.id.editBrandText);
        editLicenseplateText = (EditText)findViewById(R.id.editLicenseplate);
        editLabelText = (EditText)findViewById(R.id.editEnergyLabel);
        editYearText = (EditText)findViewById(R.id.editConstructionyearText);
        saveVehicleButton = (Button)findViewById(R.id.saveButtonAddVehicle);
        addData();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            addPictureButton.setEnabled(false);
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        }
    }

    public void addData(){
        saveVehicleButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isInserted = reptraceDb.insertVehicle(editBrandText.getText().toString(),
                        editLicenseplateText.getText().toString(),
                                editLabelText.getText().toString(),
                                editYearText.getText().toString());
                        if(isInserted) {
                            Toast.makeText(AddVehicleActivity.this,"Vehicle Added",Toast.LENGTH_LONG).show();
                        }

                        else {
                            Toast.makeText(AddVehicleActivity.this,"Vehicle Not Added",Toast.LENGTH_LONG).show();

                        }
                    }
                }
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                addPictureButton.setEnabled(true);
            }
        }
    }

    static final int REQUEST_IMAGE_CAPTURE = 1;

    public void takePictureIntent(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageViewPicture.setImageBitmap(imageBitmap);
        }
    }

    String photoPathParameters;

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        photoPathParameters = image.getAbsolutePath();
        return image;
    }

    static final int REQUEST_TAKE_PHOTO = 1;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(photoPathParameters);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void setPic() {
        int targetW = imageViewPicture.getWidth();
        int targetH = imageViewPicture.getHeight();

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoPathParameters, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(photoPathParameters, bmOptions);
        imageViewPicture.setImageBitmap(bitmap);
    }

    public void VehicleActivity (View view){
                Log.d(LOG_TAG, "Back Button Add Vehicle clicked!");
                Intent vehicleActivity = new Intent(this, VehiclesActivity.class);
                startActivity(vehicleActivity);
            }

}

